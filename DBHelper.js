const aws = require('aws-sdk');
const DB = new aws.DynamoDB.DocumentClient({
  region: 'us-east-1'
});

// A small subset of reserved words from DynamoDB
const RESERVED = require('./EscapedWords');

module.exports.add = (table, key, value, item) => {
  // Ensure the item doesn't also include the ID value
  delete item[key];

  const keys = Object.keys(item);
  let updateStatement = [];
  let attributes = {};
  let names = {};
  keys.forEach(k => {
    if (item[k]) {
      if (RESERVED.indexOf(k) > -1) {
        updateStatement.push('#' + k + ' = :' + k);
        names['#' + k] = k;
      } else {
        updateStatement.push(k + ' = :' + k);
      }
      attributes[':' + k] = item[k];
    } else {
      // Don't add empty value
    }
  });
  return DB.update({
    TableName: table,
    Key: { [key]: value },
    UpdateExpression: 'SET ' + updateStatement.join(', '),
    ExpressionAttributeValues: attributes,
    ExpressionAttributeNames: Object.keys(names).length ? names : undefined,
    ReturnValues: 'ALL_NEW'
  }).promise().then(result => {
    return result.Attributes;
  });
};

module.exports.get = (table, property, value) => {
  return DB.get({
    TableName: table,
    Key: {
      [property]: value
    }
  }).promise().then(result => {
    if (!result.Item) {
      return null;
    }
    return result.Item;
  });
};

/**
 * Performs a scan operation on the table, with an option to filter using an index.
 * @param {String} table - The table name to scan.
 * @param {String} index - An optional index to use.
 * @param {Map} filters - A map of key-value pairs used to filter the scan results.
 * @param {String} projection - A ProjectionExpression 
 */
module.exports.scan = (table, index = undefined, filters = {}, projection = '') => {
  const keys = Object.keys(filters);
  const params = {
    TableName: table,
    IndexName: index
  };
  if (keys.length) {
    let filterExpression = [];
    let attributes = {};
    keys.forEach(k => {
      filterExpression.push(`${k} = :${k}`)
      attributes[':' + k] = filters[k];
    });
    params.FilterExpression = filterExpression.join(' AND ');
    params.ExpressionAttributeValues = attributes;
  }
  if (projection) {
    params.ProjectionExpression = projection;
  }

  return DB.scan(params).promise().then(result => {
    return {
      items: result.Items,
      count: result.ScannedCount
    };
  });
};

module.exports.delete = (table, property, value) => {
  return DB.delete({
    TableName: table,
    Key: {
      [property]: value
    }
  }).promise();
};
