# DynamoHelper

This is a tiny module that eases calls to DynamoDB. It includes only the most frequently-used calls I make, along with configuration I often use. Could be useful for someone else, but I'm hosting it here for easier installation and updates between projects. :)

```
const DynamoHelper = require('dynamo-helper');
DynamoHelper.scan(...);
DynamoHelper.add(...);
DynamoHelper.get(...);
DynamoHelper.delete(...);
```

## Installation

`npm install dynamo-helper`

## Functions

### add

Calls `update` for an item. It crafts the `UpdateStatement` based on the keys and values of the `object` parameter.

Parameters:

* `tableName` (String)
* `idProperty` (String) - The name of the hash field.
* `idValue` (String) - The actual value of the id.
* `object` (Map) - The object to insert or update.

Returns:

`Promise(item)` - A promise which will resolve with the added item, or an error if one occurred.

### scan

Scans a given table. Optionally accepts an index name and filters. Currently does not support paging.

Parameters:

* `tableName` (String)
* `indexName` (String, optional) - A secondary index to use.
* `filters` (Map, optional) - The filters to be added to `FilterExpression` and `ExpressionAttributeValues`.
* `projection` (String, optional) - A `ProjectionExpression` to filter result columns.

Returns:

```
Promise({
    items: [...],
    count: 
})
```

The items are an array of scanned rows (which may be filtered). The `count` represents the TotalCount. 

### get

Get by id. 

Parameters:

* `tableName` (String)
* `idProperty` (String) - The name of the hash field.
* `idValue` (String) - The actual value of the id. 

Returns:

`Promise(item)` - A promise which will resolve with the item or `null` (if none was found).

### delete

Delete by id.

Parameters:

* `tableName` (String)
* `idProperty` (String) - The name of the hash field.
* `idValue` (String) - The actual value of the id.

Returns:

`Promise(item)` - A promise which will resolve with the deleted item.
